#!/usr/bin/env /share/share1/share_dff/anaconda3/bin/python

"""
Author: Lira Mota, lmota20@gsb.columbia.edu
Data: 2019-02
"""


def pk_integrity(df, primary_key):

    """Check that df columns in primary_key consist no missing values or duplicates."""

    assert df[primary_key].notna().all().all(), 'Null values detected in primary key.'
    assert not df[primary_key].duplicated().any(), 'Duplicate values detected in primary key.'

    pass

