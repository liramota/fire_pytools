#!/usr/bin/env /share/share1/share_dff/anaconda3/bin/python
# coding: utf-8

# %% Packages
import pandas as pd
import numpy as np
import statsmodels.formula.api as smf

# %% Function definition


def get_reg_results(reg_data, formula, cov_type='nonrobust', method='ma', cumulate=3, var_nobs=252):
    """
    Get the wanted paramenters from the OLS regression.

    Parameters:
    -----------
    formula: the OLS formula (R like)
    data: DataFrame
    #watend_parameters: list

    Return:
    -------
    Series
    """
    lhs = formula.rpartition('~')[0]
    rhs = formula.split("~")[1].split("+")
    bnames = [x.strip() for x in rhs if x.strip() not in ['1', 'RYdummy', 'Jandummy']]

    # average stats
    ave = smf.ols(lhs + '~1', reg_data).fit(cov_type=cov_type)
    results = pd.Series({'ave': ave.params['Intercept'], 't(ave)': ave.tvalues['Intercept']})

    if method == "bab":
        logret = reg_data[bnames + [lhs.strip()]]  # np.log(1+reg_data[rhs + [lhs]])

        # Volatility estimator
        std_est = np.diag(logret[-var_nobs:].std())

        ## Cumulate daily returns over n days
        cumlogret = logret.rolling(window=cumulate, center=False).sum()

        ## Correlation estimator
        corr_est = cumlogret.corr()
        ## Covariance estimator
        cov_est = np.dot(np.dot(std_est, corr_est), std_est)

        ## Beta estimator
        betas = np.dot(np.linalg.inv(cov_est[:-1, :-1]), cov_est[:-1, -1])

        residuals = logret[lhs] - np.dot(logret[rhs], betas)

        ## Name vector beta, prevent order mistakes
        betas = pd.Series(betas, index=['b' + r for r in rhs])

        betas = pd.Series(np.mean(residuals), index=['a']).append(betas)
        results = results.append(betas)

        R2 = pd.Series({'R2': 1 - residuals.var() / logret[lhs].var()})
        results = results.append(R2)

    else:
        res = smf.ols(formula, reg_data).fit(cov_type=cov_type)

        # betas
        betas = res.params
        betas.index = np.append(betas.index.values[0], 'b' + betas.index.values[1:])
        betas.rename({'Intercept': 'a'}, inplace=True)

        results = results.append(betas)

        # tstats
        tvalues = res.tvalues
        tvalues.index = 't(' + tvalues.index.values + ')'
        tvalues.rename({'t(Intercept)': 't(a)'}, inplace=True)
        results = results.append(tvalues)

        # R2
        R2 = pd.Series({'R2': res.rsquared})
        results = results.append(R2)

    return (results)
