import pandas as pd
import os
os.chdir('/Users/liramertens/src/fire_pytools/')

from data_setup.stock_annual import main as stock_annual
adata = stock_annual()
adata.to_csv('/Users/liramertens/Dropbox/Projects/financing_superstars/data/processed/compustat/stock_annual.csv')


from data_setup.stock_monthly import main as stock_monthly
mdata = stock_monthly()
mdata.to_csv('/Users/liramertens/Dropbox/Projects/financing_superstars/data/processed/compustat/stock_monthly.csv')
