#!/usr/bin/env /share/share1/share_dff/anaconda3/bin/python
# coding: utf-8

"""
Author: Lira Mota, lmota20@gsb.columbia.edu
Data: 2019-02
"""

# %% Packages
import pandas as pd
import numpy as np

from portools.find_breakpoints import find_breakpoints


# %% Auxiliary functions


def cut_data(series, q, var_name=None):
    """ Auxiliary function """
    bp = series[0:len(q)]
    bins = pd.concat([pd.Series([-np.infty]), bp, pd.Series([np.infty])])
    if pd.isnull(var_name):
        bin_names = [x for x in range(1, len(bins))]
    else:
        bin_names = [var_name + str(x) for x in range(1, len(bins))]
    flagged = pd.cut(series, bins, labels=bin_names, right=True)
    return flagged

# %% Function Definition


def sort_portfolios(data, quantiles, id_variables, breakpoints=None, exch_cd=None, silent=False, numeric_ports=False):
    """
    Sort portfolios into quantiles.

    Parameters:
    -----------
    data: DataFrame
    quantiles: dictionary
        Format is {'VARIABLE_TO_BE_SORTED_ON': [quantile]}.
    id_variables: list
        Format is ['DATE','PERMNO','EXCHCD'].
    breakpoints: DataFrame, optional
        If None, breakpoints will be calculated.
    exch_cd: list, optional
        Exchange code to use as reference for calculation of the breakpoints - usually NYSE [1].
    silent: boolean, default False
        If True, print status reports to console.
    numeric_ports: boolean, default False
        By default portfolio names carry the characteristic name and bucket number, ex. BEME1, BEME2, BEME3.
        If numeric_ports=True, portfolio names will be integers only referring the bucket number.

     Examples
    --------
    port = sort_portfolios(data = adata,
                           quantiles = {'mesum_dec':[0.5], 'beme':[0.3,0.7]},
                           id_variables = ['fyear', 'permno', 'exchcd'],
                           exch_cd = [1])
    """

    var_names = list(quantiles.keys())

    for i in range(len(var_names)):

        # Wide Table:
        pdata = data.pivot_table(values=var_names[i], index=id_variables[0], columns=id_variables[1])

        if not breakpoints:
            # Find Break-points:
            bp = find_breakpoints(data, {var_names[i]: quantiles[var_names[i]]}, id_variables, exch_cd, silent=silent)
        else:
            bp = breakpoints[var_names[i]]
        pdata = pd.merge(bp.set_index(id_variables[0]), pdata, left_index=True, right_index=True)

        # Cut data for each date:
        if numeric_ports:
            x = pdata.apply(lambda x: cut_data(series=x, q=quantiles[var_names[i]]), axis=1, result_type='reduce')
            # x = pdata.apply(lambda x: cut_data(x, quantiles[var_names[i]]), axis=1, reduce=True)
        else:
            x = pdata.apply(lambda x: cut_data(x, quantiles[var_names[i]], var_names[i]), axis=1, result_type='reduce')
            # x = pdata.apply(lambda x: cut_data(x, quantiles[var_names[i]], var_names[i]), axis=1, reduce=True)

        # Set Data to return():
        x.reset_index(inplace=True)
        x.drop(quantiles[var_names[i]], axis=1, inplace=True)

        x = pd.melt(x,
                    id_vars=id_variables[0],
                    var_name=id_variables[1],
                    value_name=var_names[i] + 'portfolio').sort_values(by=[id_variables[0],
                                                                           id_variables[1]]).reset_index(drop=True)

        x.dropna(inplace=True)
        x.reset_index(drop=True, inplace=True)

        x[id_variables[1]] = pd.to_numeric(x[id_variables[1]])

        if silent is False:
            print("Stocks were sorted according to breakpoints, starting in " + str(x.loc[0, id_variables[0]]))

        if i == 0:
            y = x
        if i > 0:
            y = pd.merge(x, y, on=id_variables[:2])

    return y
